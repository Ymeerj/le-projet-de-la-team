# Le projet de la team

## Groupe & Réalisation :

- Quentin LESPINASSE
- Sébastien RICHARD
- Jeremy DEPLACE
- Julien PERBET

Les deux sites Statique et Dynamique ont été réalisés

## Comparatif Web Statique + Web Service et Web Dynamique 
| Aspect                       | Avantages Web Statique + Web Services                            | Inconvénients Web Statique + Web Services                         | Avantages Web Dynamique                                           | Inconvénients Web Dynamique                                       |
|------------------------------|------------------------------------------------------------------|--------------------------------------------------------------------|-------------------------------------------------------------------|------------------------------------------------------------------|
| **Performance**              | Charges plus rapides car les fichiers sont servis directement.   | Si API lente, affecte la performance.                              | Peut gérer des fonctionnalités interactives sans rechargement.   | La génération dynamique des pages peut être plus lente.            |
| **Sécurité**                 | Moins de surface d'attaque sans code serveur pour la génération. | Dépendance aux services web peut introduire des vulnérabilités.   | Capable de gérer des données sensibles avec des contrôles robustes. | Plus de code serveur peut augmenter les risques de sécurité.     |
| **Simplicité et Coûts**      | Simple à développer et à déployer, coûts de serveur réduits.     | Limité pour les sites nécessitant une interaction complexe.       | Gestion de contenu facilitée avec des CMS.                         | Complexité de développement et infrastructure plus coûteuse.    |
| **Scalabilité**              | Facilement scalable via des CDN.                                  | Besoin de services externes pour la scalabilité fonctionnelle.    | Scalabilité dans la gestion des utilisateurs et des données.      | Nécessite une gestion de charge serveur plus complexe.           |
| **Fonctionnalités**          | Suffisant pour des sites de contenu fixe.                         | Fonctionnalités dynamiques limitées sans intégration d'API.       | Supporte des fonctionnalités complexes et personnalisées.         | Peut devenir lourd avec trop de fonctionnalités intégrées.       |
| **Dépendance aux API**       | Extensible via des API pour des fonctionnalités dynamiques.      | Hautement dépendant de la fiabilité des API externes.             | Indépendant des API externes pour les fonctionnalités de base.   | Moins dépendant mais peut nécessiter des intégrations complexes. |
| **Gestion des Contenus**     | Nécessite des mises à jour manuelles ou des outils spécifiques.   | Pas idéal pour les utilisateurs non techniques sans outils de CMS. | CMS permet des mises à jour faciles et rapides du contenu.        | Complexité accrue dans la gestion des contenus dynamiques.       |

## Réponse aux questions : 

### - Qu’est-ce que le pattern MVC ? 

Le pattern **MVC** (Modèle-Vue-Contrôleur) est un motif de conception utilisé en programmation informatique pour séparer les données d'une application, l'interface utilisateur et la logique de contrôle en trois composants distincts : 

- **Modèle** : Gère les données et la logique métier de l'application. Il communique avec la base de données et traite les données.

- **Vue** : Affiche les données, c'est-à-dire l'interface utilisateur que les utilisateurs interagissent avec. Elle reflète les changements dans le modèle et présente une représentation visuelle des données.

- **Contrôleur** : Agit comme un intermédiaire entre le modèle et la vue, contrôlant la façon dont les données sont présentées et manipulées. Il répond aux entrées de l'utilisateur, en les interprétant et en décidant des actions à prendre.

### - Quels avantages présente-t-il ? Qu’est-ce que le Web dynamique ? pourquoi est-il intéressant ? 
**Avantages** : 

- **Séparation des préoccupations** : Chaque partie du MVC peut être développée indépendamment des autres, ce qui simplifie le design, le développement et les tests.
- **Facilité de maintenance** : Le code est plus organisé et plus facile à mettre à jour ou à adapter.
- **Développement en équipe** : Facilite le travail collaboratif puisque différentes équipes peuvent travailler sur les composants en parallèle sans interférer les unes avec les autres.
- **Réutilisabilité** : Le code modulaire permet de réutiliser des parties pour d'autres application

Le **Web dynamique** fait référence à des sites Web où le contenu peut changer dynamiquement en fonction des interactions de l'utilisateur, du contexte, ou des données en temps réel. Contrairement aux sites statiques, le contenu des pages web dynamiques est généré à la volée.

Il est intéressant pour trois aspects : 
- **Personnalisation** : Adaptation du contenu aux préférences et aux comportements des utilisateurs.
- **Interactivité** : Les utilisateurs peuvent interagir avec le contenu, par exemple, remplir des formulaires, voter, et commenter.
- **Fonctionnalités riches** : Prise en charge de systèmes complexes tels que les boutiques en ligne, les forums ou les réseaux sociaux.

### - Comment sont récupérés les fichiers par le Web Browser en Web statique ? 
En web statique, lorsque l'utilisateur demande une page, le serveur web répond directement avec les fichiers HTML, CSS, et JavaScript **stockés sur le serveur**. Ces fichiers sont transférés via **HTTP/HTTPS** au navigateur de l'utilisateur, qui interprète le HTML et le CSS pour afficher la page et exécute le JavaScript pour toute interactivité client-side.


### - Quels sont les avantages d’utiliser du Web Statique avec des services REST ?
**Avantages** : 
- **Performance** : Les pages statiques se chargent plus rapidement car elles ne nécessitent pas de traitement côté serveur pour générer la page à chaque requête.
- **Sécurité**: Moins de risques de failles de sécurité, car il n'y a pas d'interaction avec les bases de données ou les systèmes de gestion de contenu côté serveur.
- **Scalabilité** : Plus facile à mettre à l'échelle car les fichiers peuvent être distribués via des réseaux de distribution de contenu (CDN).
- **Coûts** : Moins coûteux en termes de ressources serveur.
- **Flexibilité** : L'utilisation de services REST permet d'intégrer des fonctionnalités dynamiques au besoin, offrant ainsi une bonne balance entre performance et interactivité.
### - Qu’est que les architectures N-Tiers ? Quelles avantages apportent-elles ?
Les **architectures N-Tiers** (ou architectures multi-niveaux) sont des modèles de conception de logiciels qui divisent une application en **niveaux ou couches** distincts. Chaque couche a un rôle spécifique et distinct, et communique avec les couches directement adjacentes. Une application **N-Tiers** typique est structurée de manière à ce que chaque couche soit **physiquement séparée** des autres, favorisant ainsi la **modularité** et la **flexibilité**.

**Avantages** :
- **Scalabilité** : Facile à adapter en montée en charge, car vous pouvez ajouter plus de ressources ou des instances supplémentaires à des couches spécifiques en fonction des besoins.
- **Maintenabilité** : Les modifications ou les mises à jour peuvent être effectuées sur une couche spécifique sans perturber les autres couches de l'application, ce qui simplifie la maintenance et les mises à niveau.
- **Flexibilité** : Chaque couche peut être développée en utilisant la technologie la mieux adaptée à ses besoins spécifiques.
- **Réutilisation** : Les couches peuvent être réutilisées par d'autres applications ou dans d'autres architectures au sein de la même organisation.
- **Sécurité améliorée** : Les responsabilités de sécurité peuvent être clairement définies et gérées à chaque couche, permettant des stratégies de défense en profondeur.
- **Distribution** : Les couches peuvent être distribuées physiquement sur différents serveurs ou même à travers des centres de données, améliorant ainsi la performance et la fiabilité.

### - Comment fonctionne l’AJAX ?
**AJAX (Asynchronous JavaScript and XML)** est une technique de développement web utilisée pour créer des applications **interactives et dynamiques**. AJAX permet aux pages web de récupérer des données du serveur et de mettre à jour des parties de la page web sans nécessiter de rechargement complet de la page. Le fonctionnement d’AJAX s'articule autour de quelques composants clés :

- **XMLHttpRequest** : C'est l'objet JavaScript qui permet la communication asynchrone avec le serveur. Les développeurs peuvent envoyer des requêtes HTTP pour récupérer des données sans interrompre l'expérience utilisateur.
- **JavaScript et DOM** : JavaScript est utilisé pour manipuler le DOM (Document Object Model) de la page en fonction des données reçues, permettant ainsi la mise à jour dynamique de la page.
- **Formats de données **: Bien que l'acronyme inclut XML, AJAX peut travailler avec d'autres formats de données comme JSON, qui est plus léger et facile à manipuler avec JavaScript.
### - Qu’est-ce que JEE ? Comment fonctionne un serveur JEE ? Qu’est-ce qu’un Web Container en JEE ?
Java EE (Enterprise Edition), rebaptisé Jakarta EE, est une plateforme standardisée pour le développement d'applications d'entreprise en Java. Elle étend les capacités de Java SE avec des bibliothèques pour les systèmes répartis et les services web. Java EE inclut des spécifications pour des APIs comme les Servlets, JSP, EJB, JPA, et JSF, qui facilitent le développement d'applications robustes, évolutives et sécurisées.

Un serveur JEE, comme GlassFish, WildFly (anciennement JBoss), ou Apache TomEE, sert de plateforme pour exécuter les applications développées avec la technologie Java EE. Il gère les aspects suivants :

- **Services de conteneur Web et EJB** : Pour l'exécution de servlets, JSP et enterprise Java beans.
- **Gestion des transactions** : Assure que les opérations de base de données soient complètes et correctes.
- **Sécurité** : Gère l'authentification, l'autorisation, et le chiffrement.
- **Intégration et gestion de ressources** : Connecte avec des bases de données, des systèmes de messagerie, etc.

Un conteneur web en JEE est une partie du serveur d'applications qui gère l'exécution des servlets et des pages JSP. Il prend en charge le cycle de vie des servlets, la gestion des requêtes et des réponses HTTP, et assure que les requêtes sont traitées selon les spécifications des servlets.
### - Qu’est ce que Springboot ? quelles différences avec JEE ?
**Spring Boot** est un projet au sein de l’écosystème Spring qui simplifie le processus de configuration et de publication des applications basées sur Spring. 

Il permet aux développeurs de créer des applications **stand-alone** qui peuvent être démarrées avec très peu de configuration initiale. Spring Boot offre également une intégration extensive avec des outils de développement, des systèmes de gestion de bases de données, et des infrastructures de cloud computing.

- **Configuration** : Spring Boot vise à réduire la configuration nécessaire à l'aide de l'auto-configuration, tandis que JEE nécessite souvent des configurations manuelles plus détaillées.
- **Dépendances** : Spring Boot utilise une approche de "starter dependencies" pour inclure automatiquement les bibliothèques nécessaires, contrairement à JEE qui peut requérir des fichiers de configuration XML ou des annotations dispersées.
- **Agilité et microservices** : Spring Boot est particulièrement adapté pour développer des microservices rapidement, une approche moderne en contraste avec les monolithes d'entreprise souvent associés à JEE.
### - Qu’apporte Thymeleaf à Springboot ?
**Thymeleaf** apporte à Spring Boot une intégration profonde avec les fonctionnalités de Spring pour le développement de vues côté serveur, facilitant la création de pages HTML dynamiques. 

Il offre une syntaxe claire et intuitive qui améliore la lisibilité et la maintenance des templates. Thymeleaf assure également la sécurité en prévenant les **attaques XSS** et permet le développement rapide avec la possibilité de prévisualiser les pages HTML en mode hors ligne. En bref, il rend le développement d'applications web plus **efficace et sécurisé** au sein de l'écosystème Spring Boot.

### - Que signifie l’annotation @Controller, pourquoi est-elle importante lors de la génération de pas coté serveur ?
L'annotation @Controller dans Spring Framework est utilisée pour indiquer qu'une classe particulière sert de contrôleur dans le modèle **MVC (Modèle-Vue-Contrôleur)**. Cette annotation est une spécialisation de @Component, ce qui permet à Spring de détecter automatiquement les classes de contrôle lors de l'analyse des paquets pour les composants. Voici pourquoi elle est importante pour la génération de pages côté serveur :

- **Définition du Rôle** : @Controller signale que la classe est destinée à être un contrôleur web, ce qui aide Spring à assigner des responsabilités spécifiques liées à la manipulation des requêtes HTTP.
- **Gestion des Requêtes** : Les contrôleurs définis avec @Controller sont responsables de traiter les requêtes entrantes, de gérer la logique métier, et de retourner une réponse au client, souvent sous forme de vue générée par le serveur.
- **Mapping des Requêtes** : À l'intérieur d'une classe contrôleur, vous pouvez utiliser d'autres annotations comme @RequestMapping ou @GetMapping pour associer des méthodes spécifiques à des URI (adresses web). Cela permet de définir comment différentes requêtes HTTP sont gérées.
- **Intégration des Modèles** : Un contrôleur peut ajouter des attributs à un modèle, qui sont ensuite accessibles dans les vues générées, comme des pages HTML dynamiques.
- **Support de Vue** : En utilisant @Controller, Spring peut intégrer des moteurs de template comme Thymeleaf pour générer des réponses HTML en réponse à des requêtes, ce qui est essentiel pour les applications web qui génèrent des vues côté serveur.
### - Que représente le répertoire ‘src/main/resources’ dans un projet SpringBoot ? Quelles sont les informations stockées à cet endroit ?
Dans un projet Spring Boot, le répertoire src/main/resources joue un rôle central pour stocker des ressources non-code, et il contient généralement :

- **Fichiers de Configuration** : Des fichiers comme application.properties ou application.yml, qui configurent des aspects de l'application Spring Boot, comme la connexion à la base de données, les configurations spécifiques au framework, et d'autres variables d'environnement.
- **Fichiers Statiques** : Ressources comme des images, des fichiers JavaScript, et des feuilles de style CSS qui sont utilisées par l'application.
- **Templates de Vues** : Fichiers de template pour des moteurs comme Thymeleaf, utilisés pour générer les vues HTML côté serveur.
Internationalisation : Fichiers de propriétés pour la localisation, tels que messages.properties, qui permettent de supporter différentes langues et régionalisations.
- **Fichiers SQL **: Scripts SQL pour la création de bases de données, les migrations, ou pour charger des données de départ dans la base de données.
- **Autres Ressources** : Comme les fichiers XML pour la configuration de Spring ou d'autres frameworks intégrés.


Ce répertoire est crucial parce qu'il **sépare les ressources** qui ne sont pas compilées du code source, permettant ainsi une gestion plus claire et une meilleure organisation du projet. Les ressources placées ici sont automatiquement incluses dans le classpath de l'application, rendant leur accès plus simple et direct.
