import config from '../conf.js';

export class ApiService {
  constructor(controllerName) {
    this.link = config.API_URL + controllerName;
  }
}
