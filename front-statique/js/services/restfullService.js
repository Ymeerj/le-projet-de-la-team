import { ApiService } from './apiService.js';

export class RestFullService extends ApiService {
  constructor(params) {
    super(params);
  }

  getList() {
    return fetch(this.link).then((response) => response.json());
  }

  getById(id) {
    return fetch(`${this.link}/${id}`).then((response) => response.json());
  }

  create(card) {
    return fetch(this.link, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(card),
    }).then((response) => response.json());
  }

  update(card) {
    return fetch(`${this.link}/${card.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(card),
    }).then((response) => response.json());
  }

  delete(id) {
    return fetch(`${this.link}/${id}`, {
      method: 'DELETE',
    }).then((response) => response.json());
  }
}
