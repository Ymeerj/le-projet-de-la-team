import { ApiService } from './apiService.js';
import { RestFullService } from './restfullService.js';

export default class CardService extends RestFullService {
  constructor() {
    super('card');
  }
}
