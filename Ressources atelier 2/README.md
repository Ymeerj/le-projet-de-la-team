# Le projet de la team

## Groupe & Réalisation :

- Quentin LESPINASSE
- Sébastien RICHARD
- Jeremy DEPLACE
- Julien PERBET

## État du projet :

**Projet construit** en suivant l'architecture proposé dans les diagrammes en ressources, cela inclut :
- Interface de connexion et de création de compte
- Interface pour afficher les cartes dans notres inventaire
- Interface pour afficher les cartes disponibles à l'achat sous forme de liste
- Interface pour afficher les cartes que l'on souhaite vendre avec la possibilité de choisir son propre prix
- Logique de vente et achat de carte -> on vérifie si on est bien connecté
- Création d'user et login
- Gestion des exceptions
- Sécurité avec les tokens en cookies pour la gestion des entry points des APIs
- Une initialisation de la base de données ( dans **"DataInitializer"**), où l'on défini également des cartes de "base" pour notre base de données, elle est améliorable mais à cause de la contrainte de temps cela reste sommaire

- La bdd s'instancie grâce à un **docker**, le docker compose dans le dossier **"dynamique"** est à lancer afin de pouvoir tester l'application
les identifiants de la base de données sont spécifiés dans le ficier **"application.properties"** dans le dossier **"ressources"** :

```
spring.datasource.url=jdbc:mysql://localhost:3306/mydatabase
spring.datasource.username=user
spring.datasource.password=password
```

![My Image](img\connexionBdd.png)
