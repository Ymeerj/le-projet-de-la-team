INSERT INTO user (first_name, last_name, password) VALUES ('John', 'Doe', 'password1');
INSERT INTO user (first_name, last_name, password) VALUES ('Jane', 'Doe', 'password2');

INSERT INTO card (name, description) VALUES ('Card1', 'Description1');
INSERT INTO card (name, description) VALUES ('Card2', 'Description2');

INSERT INTO card_owned_by (card_id, owner_id) VALUES (1, 1);
INSERT INTO card_owned_by (card_id, owner_id) VALUES (2, 1);
INSERT INTO card_owned_by (card_id, owner_id) VALUES (1, 2);