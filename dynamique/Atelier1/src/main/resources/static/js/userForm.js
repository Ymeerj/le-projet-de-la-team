function process(elt){
    
    let firstName = document.getElementsByName('first-name');
    let lastName = document.getElementsByName('last-name');
    let password = document.getElementsByName('password');
    let rePassword = document.getElementsByName('re-password');

    data = {"firstName":firstName[0].value, "lastName":lastName[0].value, "password":password[0].value}
    console.log(data);
}

function check(input){
    console.log("checked");
}


document.getElementById('signupForm').addEventListener('submit', async function (e) {
        e.preventDefault();

        const name = document.getElementById('first-name').value;
        const surname = document.getElementById('last-name').value;
        const password = document.getElementById('password').value;
        const rePassword = document.getElementById('re-password').value;

        if (password !== rePassword) {
            alert("Passwords do not match!");
            console.log(" password !")
            return;
        }

        const formData = {
            firstName: name,
            lastName: surname,
            password: password
        };

        try {
            const response = await fetch('http://localhost:8080/api/auth/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });

            if (!response.ok) {
                throw new Error("Network response was not ok");
            }

            const result = await response.json();
            console.log(result);
            alert("User registered successfully!");
        } catch (error) {
            console.error("There was a problem with the registration request:", error);
        }
    });
function resetForm() {
    document.getElementById('signupForm').reset();
}
