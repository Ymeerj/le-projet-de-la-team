var cookies = undefined

window.onload = () =>{

    cookies = document.cookie.split('; ').reduce((prev, current) => {
        const [name, ...value] = current.split('=');
        prev[name] = value.join('=');
        return prev;
    }, {});

    if(cookies.token == null) window.location.href = 'http://localhost:8080/connexion/loginUser.html'

    fetch(`http://localhost:8080/user/${cookies.token}`)
        .then(response =>{
            if(response.ok){
                return response.json()
            }

            return Promise.reject(response)
        })
        .then(data => {
            document.getElementById('walletId').innerHTML = data.credits
            document.getElementById('userNameId').innerHTML = data.firstName;
        })
        .catch((response) =>{
            if(response.status === 404) {
                window.location.href = `http://${window.location.host}/connexion/loginUser.html`
            }
        })
}