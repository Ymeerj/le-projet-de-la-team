let cardList = []
let template = document.querySelector("#row");

let cookies2 = document.cookie.split('; ').reduce((prev, current) => {
    const [name, ...value] = current.split('=');
    prev[name] = value.join('=');
    return prev;
}, {});


fetch(`http://localhost:8080/api/cards/user_cards/${cookies2.token}`).then((response)=>{
    response.json().then((data => {
        for(const cardOwnedBy of data){
            let card = cardOwnedBy.card
            let clone = document.importNode(template.content, true);


            newContent= clone.firstElementChild.innerHTML
                .replace(/{{family_src}}/g, "")
                .replace(/{{family_name}}/g, card.family)
                .replace(/{{img_src}}/g, card.image_url)
                .replace(/{{name}}/g, card.name)
                .replace(/{{description}}/g, card.description)
                .replace(/{{hp}}/g, card.hp)
                .replace(/{{energy}}/g, card.energy)
                .replace(/{{attack}}/g, card.attack)
                .replace(/{{defense}}/g, card.defense)
                .replace(/{{id}}/g, cardOwnedBy.id)
            clone.firstElementChild.innerHTML= newContent;

            let cardContainer= document.querySelector("#tableContent");
            cardContainer.appendChild(clone);
        }
    }))
})


function sellCard (id) {
    let price = prompt("Entrer le prix de vente", "30");
    if (price == null || price == "") {
        return
    } else {
        let body = {
            "cardId": id,
            "price": price
        }
        fetch(`http://${window.location.host}/market/sell_card`, {method: "POST", body: JSON.stringify(body), headers:{"Content-Type": "application/json"}})
            .then((response)=>{
                console.log(response.ok);

                if(response.ok){
                    location.reload()
                } else {
                    alert(" Une erreur: lors de la vente de la carte ! Essayé de vous connecter avec un token ! ")
                    window.location.href = '/connexion/loginUser.html';
                }


            })
    }


}







