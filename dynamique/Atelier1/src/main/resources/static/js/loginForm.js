document.getElementById('loginForm').addEventListener('submit', async function (e) {
    e.preventDefault();

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    const loginData = {
        firstName: username,  // Assurez-vous que cela correspond à ce que le backend attend
        password: password
    };

    try {
        const response = await fetch('http://localhost:8080/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginData)
        });


        if (!response.ok) {

            const errorText = await response.text();
            alert("Login Failed!");

            return;
        }

        const result = await response.json();
        console.log(result);

        if (Number.isInteger(result)) {
            alert("Login successful!");
            window.location.href = '/home.html';
        } else {
            alert("Login Failed!");
        }
    } catch (error) {
        console.error("There was a problem");
        alert("Login Failed!");
    }
});
