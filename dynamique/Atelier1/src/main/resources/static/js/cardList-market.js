let cardList = []


document.addEventListener("DOMContentLoaded", (event)=> {
    fetch("http://localhost:8080/market").then(result => result.json().then((data)=>{
        cardList = data;
        setTemplate("#cardlist","#tableContent",cardList)
    }))
})
function onProcess(transactionId){
    let body = {
        "buyerId": cookies.token,
        "transactionId": transactionId
    }

    fetch(`http://${window.location.host}/market/buy_card`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body)
    }).then((response) => {
        if(response.ok){
            location.reload()
        } else {
            alert("Une erreur est survenu, ")
            window.location.href = '/connexion/loginUser.html';
        }    })
}

setCardlist()







