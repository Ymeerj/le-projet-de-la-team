document.getElementById('signupForm').addEventListener('submit', async function(e) {
    e.preventDefault();

    const name = document.getElementById('first-name').value;
    const surname = document.getElementById('last-name').value;
    const password = document.getElementById('password').value;
    const rePassword = document.getElementById('re-password').value;

    if (password !== rePassword) {
        alert("Passwords do not match!");
        return;
    }


    const formData = {
        firstName: name,
        lastName: surname,
        password: password
    };

    try {
        const response = await fetch('http://localhost:8080/api/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });

        if (!response.ok) {
            const errorText = await response.text();
            alert("Login Failed!");
            return;
        }

        const result = await response.json();
        alert("User registered successfully!");
    } catch (error) {
        console.error("There was a problem with the registration request:", error);
    }
});

function resetForm() {
    document.getElementById('signupForm').reset();
}
