package com.sp.exception;

public class CreditException extends RuntimeException{
    public CreditException(String msg)
    {
        super(msg);
    }
}
