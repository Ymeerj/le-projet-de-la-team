package com.sp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RessourceNotFoundException.class)
    public ResponseEntity<String> ressourceNotFoundException(RessourceNotFoundException e, WebRequest request)
    {
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(RessouceAlreadyExistsException.class)
    public ResponseEntity<String> ressourceAlreadyExists(RessouceAlreadyExistsException e, WebRequest request)
    {
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(CreditException.class)
    public ResponseEntity<String> creditException(CreditException e)
    {
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.FORBIDDEN);
    }
}
