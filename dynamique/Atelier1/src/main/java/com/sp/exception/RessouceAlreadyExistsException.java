package com.sp.exception;

public class RessouceAlreadyExistsException extends RuntimeException{
    public RessouceAlreadyExistsException(String msg){
        super(msg);
    }
}
