package com.sp.exception;

public class RessourceNotFoundException extends RuntimeException{
    public RessourceNotFoundException(String msg)
    {
        super(msg);
    }
}
