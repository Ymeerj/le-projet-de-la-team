package com.sp.controller;

import com.sp.dto.BuyCardDTO;
import com.sp.dto.SellCardDTO;
import com.sp.model.Card;
import com.sp.model.CardOwnedBy;
import com.sp.model.Transaction;
import com.sp.model.User;
import com.sp.service.CardService;
import com.sp.service.MarketService;
import com.sp.service.UserService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("market")
public class MarketController {

    private final MarketService marketService;

    private HttpServletRequest httpServletRequest;
    private CardService cardService;
    private UserService userService;

    public MarketController(MarketService marketService, HttpServletRequest httpServletRequest, CardService cardService , UserService userService){

        this.marketService = marketService;
        this.httpServletRequest = httpServletRequest;
        this.cardService = cardService;
        this.userService = userService;
    }

    @GetMapping(produces = "application/json")
    public List<Transaction> getCardsOnSale()
    {
        List<Transaction> cards = marketService.getCardsOnSale();
        return cards;
    }


    @PostMapping("/sell_card")
    public ResponseEntity sellCard(@RequestBody SellCardDTO sellCardDTO)
    {
        String token = null;
        User userLogin = null;
        if (this.httpServletRequest.getCookies() != null) {
            for (Cookie cookie : this.httpServletRequest.getCookies()) {
                if (cookie.getName().equals("token")) {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        if(token != null){
            Long userIdLogin = Long.parseLong(token);
            userLogin = userService.getUser(userIdLogin);
        }
        if( userLogin != null){
            System.out.println(" Vente " + sellCardDTO.getCardId());

            return ResponseEntity.ok( marketService.addSale(sellCardDTO.getCardId(), sellCardDTO.getPrice()));


        }else{
            return ResponseEntity.badRequest().build();
        }

    }

    @DeleteMapping("/remove_sale/{id}")
    public ResponseEntity removeSale(@PathVariable("id") Long transactionId){
        marketService.removeSale(transactionId);

        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/sales/{id}")
    public List<Transaction> getUserSales(@PathVariable("id") Long sellerId)
    {
        return marketService.getUserSales(sellerId);
    }

    @PostMapping("/buy_card")
    public ResponseEntity buyCard(@RequestBody BuyCardDTO buyCardDTO)
    {
        String token = null;
        User userLogin = null;
        if (this.httpServletRequest.getCookies() != null) {
            for (Cookie cookie : this.httpServletRequest.getCookies()) {
                if (cookie.getName().equals("token")) {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        if(token != null){
            Long userIdLogin = Long.parseLong(token);
            userLogin = userService.getUser(userIdLogin);
        }
        if( userLogin != null){
            return ResponseEntity.ok(marketService.buyCard(buyCardDTO));


        }else{
            return ResponseEntity.badRequest().build();
        }

    }
}
