package com.sp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sp.model.User;
import com.sp.service.AuthService;
import com.sp.service.CardService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private AuthService authService;
    @Autowired
    private CardService cardService;

    public AuthController(AuthService authService, CardService cardService)
    {
        this.authService = authService;
        this.cardService = cardService;
    }

    @PostMapping("/register")
    public ResponseEntity<Long> registerUser(@RequestBody User user, HttpServletResponse response) {

        user = authService.registerUser(user);

        if (user != null) {
            Cookie cookie = new Cookie("token", user.getId().toString());
            cookie.setPath("/");
            response.addCookie(cookie);
            cardService.assignRandomCardsToUser(user.getId());
            return ResponseEntity.ok(user.getId());
        }
        return ResponseEntity.status(401).body(null);
    }

    @PostMapping("/login")
    public ResponseEntity<Long> loginUser(@RequestBody User userLog, HttpServletResponse response) {

        User user = authService.loginUser(userLog.getFirstName(), userLog.getPassword());

        if (user != null) {
            Cookie cookie = new Cookie("token", user.getId().toString());
            cookie.setPath("/");
            response.addCookie(cookie);

            return ResponseEntity.ok(user.getId());
        }
        return ResponseEntity.status(401).body(null);
    }
}
