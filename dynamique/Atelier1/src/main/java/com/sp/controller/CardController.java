package com.sp.controller;

import com.sp.model.*;
import com.sp.repository.CardOwnedByRepository;
import com.sp.service.CardService;
import com.sp.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.servlet.http.Cookie;

import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class CardController {

    @Autowired
    private CardService cardService;

    private HttpServletRequest httpServletRequest;
    private UserService userService;

    public CardController(CardService cardService, HttpServletRequest httpServletRequest, UserService userService) {
        this.cardService = cardService;
        this.httpServletRequest = httpServletRequest;
        this.userService =userService;
    }

    @PostMapping("/create")
    public ResponseEntity<Card> createCard(@RequestBody Card card) {
        return ResponseEntity.ok(cardService.createCard(card));
    }

    @PostMapping("/sell")
    public ResponseEntity<Void> sellCard(@RequestParam Long userId, @RequestParam Long cardId) {

        String token = null;
        User userLogin = null;
        if (this.httpServletRequest.getCookies() != null) {
            for (Cookie cookie : this.httpServletRequest.getCookies()) {
                if (cookie.getName().equals("token")) {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        if(token != null){
           Long userIdLogin = Long.parseLong(token);
           userLogin = userService.getUser(userIdLogin);
        }
        if( userLogin != null){
            cardService.sellCard(userId, cardId);
            return ResponseEntity.ok().build();

        }else{
           return ResponseEntity.badRequest().build();
        }



    }

    @GetMapping("/user_cards/{userId}")
    public ResponseEntity<List<CardOwnedBy>> getCardFromUser(@PathVariable("userId") Long id)
    {
        List<CardOwnedBy> cards = cardService.getUserCard(id);

        return ResponseEntity.ok(cards);
    }
}