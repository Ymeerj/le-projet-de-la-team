package com.sp.dto;

public class SellCardDTO {
    private Long cardId;
    private int price;

    public Long getCardId() {
        return cardId;
    }

    public int getPrice() {
        return price;
    }
}
