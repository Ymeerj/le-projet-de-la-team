package com.sp.dto;

public class BuyCardDTO {

    private Long buyerId;
    private Long transactionId;

    public Long getBuyerId() {
        return buyerId;
    }

    public Long getTransactionId() {
        return transactionId;
    }
}
