package com.sp.model;

import jakarta.persistence.*;

@Entity
public class CardOwnedBy {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    private Long id;

    @ManyToOne
    private Card card;

    @ManyToOne
    private User owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardOwnedBy(Card card, User owner) {
        this.owner = owner;
        this.card = card;
    }

    public CardOwnedBy() {
        this.card = null;
        this.owner = null;
    }

    public User getOwner() {
        return owner;
    }

    public Card getCard() {
        return card;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
