package com.sp.model;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Random;


@Entity
public class Card implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
    private String image_url;
    private String family;
    private Integer hp;
    private Integer energy;
    private Integer attack;
    private Integer defense;
    private Random random= new Random();

    public Card() {
        this.name = "MissingNo";
        this.description = "Zzzz";
        this.image_url = "https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg";
        this.family = "No family";

        this.hp = random.nextInt(500) + 1;
        this.energy = random.nextInt(200) + 1;
        this.attack = random.nextInt(50) + 1;
        this.defense = random.nextInt(40) + 1;
    }
    public Card(String name, String description, String image_url, String family, Integer hp, Integer energy, Integer attack, Integer defense) {
        this.name = name;
        this.description = description;
        this.image_url = image_url;
        this.family = family;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defense = defense;
    }

    public Card(String name, String description, String image_url) {
        this.name = name;
        this.description =description;
        this.image_url = "https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg";
        this.family = "No family";
        this.hp = 5;
        this.energy = 5;
        this.attack = 5;
        this.defense = 5;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAttack() {
        return attack;
    }

    public Integer getDefense() {
        return defense;
    }

    public Integer getEnergy() {
        return energy;
    }

    public Integer getHp() {
        return hp;
    }

    public String getDescription() {
        return description;
    }

    public String getFamily() {
        return family;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}


