package com.sp.model;

public class TrainFormDTO {

    private String color;
    private int size;
    private String name;
    private String imgUrl;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public TrainFormDTO() {
        this.color = "";
        this.size = 0;
        this.name = "";
        this.imgUrl = "";
    }

    public TrainFormDTO(String name, String color, int size, String imgUrl) {
        this.color = color;
        this.size = size;
        this.name = name;
        this.imgUrl = imgUrl;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

// GETTER AND SETTER

}