package com.sp.repositories;


import com.sp.model.Transaction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MarketRepository {
    private List<Transaction> cardsOnSale;

    public MarketRepository(){
        ArrayList listCards = new ArrayList();
        this.cardsOnSale = listCards;
    }

    public List<Transaction> getCardsOnSale() {
        return this.cardsOnSale;
    }
}
