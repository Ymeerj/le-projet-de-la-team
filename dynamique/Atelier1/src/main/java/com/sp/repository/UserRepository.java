package com.sp.repository;

import com.sp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByFirstName(String firstName);

    Optional<User> findById(Long id);

    User findByFirstNameAndLastName(String firstname, String lastname);


}