package com.sp.repository;

import com.sp.model.Card;
import com.sp.model.Train;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {
    Card findByName(String name);

    Card findCardByName(String name);
}