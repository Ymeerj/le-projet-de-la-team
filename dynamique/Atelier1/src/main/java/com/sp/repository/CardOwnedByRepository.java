package com.sp.repository;

import com.sp.model.Card;
import com.sp.model.CardOwnedBy;
import com.sp.model.Train;
import com.sp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CardOwnedByRepository extends JpaRepository<CardOwnedBy, Long> {

    public List<CardOwnedBy> findByOwnerId(Long id);
   List<CardOwnedBy> findCardOwnedByCardIdAndOwnerId(Long cardId, Long userId);


}
