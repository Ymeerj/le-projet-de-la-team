package com.sp.repository;

import com.sp.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Transaction findByBuyerIdAndCardId(Long userId, Long cardId);

    List<Transaction> findByBuyerIsNull();

    List<Transaction> findBySellerIdAndBuyerIsNull(Long id);

    Transaction findByCardIdAndBuyerIsNull(Long cardId);

    Optional<Transaction> findById(Long id);
}