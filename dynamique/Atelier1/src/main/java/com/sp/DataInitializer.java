package com.sp;

import com.sp.model.Card;
import com.sp.model.User;

import com.sp.service.CardService;
import com.sp.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

@Configuration
public class DataInitializer {

    @Autowired
    private UserService userService;

    @Autowired
    private CardService cardService;


    @Bean
    CommandLineRunner initDatabase() {
        return args -> {
            // Creating Users
            User user1 = userService.createUserIfNotExists("John", "Doe", "password1");
            User user2 = userService.createUserIfNotExists("Jane", "Doe", "password2");

            // Creating Cards
            Card card1 = cardService.createCardIfNotExists("T1", "Description1","https://www.lyonmag.com/media/images/35894106182_3d32ae3169_o1.jpg");
            Card card2 = cardService.createCardIfNotExists("T2", "Description2", "https://www.lyonmag.com/media/images/38449247384_5ab4a34e26_o.jpg");
            Card card3 = cardService.createCardIfNotExists("T3", "Description3","https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Alstom_Citadis_302_n%C2%B0854_TCL_D%C3%A9cines_Grand_Large.jpg/220px-Alstom_Citadis_302_n%C2%B0854_TCL_D%C3%A9cines_Grand_Large.jpg");
            Card card4 = cardService.createCardIfNotExists("T4", "Description2", "https://static.actu.fr/uploads/2023/09/tramway-t4-lyon.jpg");
            Card card5 = cardService.createCardIfNotExists("Metro A", "Description3","https://www.lyoncapitale.fr/wp-content/uploads/2021/07/Metro-A-Debourg-@WilliamPham-1-scaled.jpg");


            // Assigning cards to users
            cardService.assignCardToUserIfNotExists(card1, user1);
            cardService.assignCardToUserIfNotExists(card2, user1);
            cardService.assignCardToUserIfNotExists(card1, user2);
            cardService.assignCardToUserIfNotExists(card3,user2);
        };
    }
}
