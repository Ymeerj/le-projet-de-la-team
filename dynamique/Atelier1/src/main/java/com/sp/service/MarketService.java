package com.sp.service;

import com.sp.dto.BuyCardDTO;
import com.sp.exception.CreditException;
import com.sp.exception.RessouceAlreadyExistsException;
import com.sp.exception.RessourceNotFoundException;
import com.sp.model.CardOwnedBy;
import com.sp.model.Transaction;
import com.sp.model.User;
import com.sp.repositories.MarketRepository;
import com.sp.repository.CardOwnedByRepository;
import com.sp.repository.TransactionRepository;
import com.sp.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class MarketService {
    private TransactionRepository transactionRepository;
    private final CardOwnedByRepository cardOwnedByRepository;
    private final UserRepository userRepository;

    public MarketService(TransactionRepository transactionRepository,
                         CardOwnedByRepository cardOwnedByRepository,
                         UserRepository userRepository) {
        this.transactionRepository = transactionRepository;
        this.cardOwnedByRepository = cardOwnedByRepository;
        this.userRepository = userRepository;
    }

    public List<Transaction> getCardsOnSale ()
    {
        return transactionRepository.findByBuyerIsNull();
    }

    public List<Transaction> getUserSales(Long sellerId)
    {
        return transactionRepository.findBySellerIdAndBuyerIsNull(sellerId);
    }

    public Transaction addSale(Long  cardOwnedById, int price) {

        CardOwnedBy card = cardOwnedByRepository.
                findById(cardOwnedById).
                orElseThrow(()-> new RessourceNotFoundException("La carte n'existe pas"));

        if (transactionRepository.findByCardIdAndBuyerIsNull(cardOwnedById) != null){
            throw new RessouceAlreadyExistsException("la carte est déjà mise en vente");
        }


        Transaction transaction = new Transaction();
        transaction.setCard(card);
        transaction.setSeller(card.getOwner());
        transaction.setPrice(price);
        transactionRepository.save(transaction);

        return transaction;
    }

    public void removeSale(Long transactionId)
    {
        Transaction transaction = transactionRepository
                .findById(transactionId)
                .orElseThrow(()->new RessourceNotFoundException("La transaction n'existe pas"));

        transactionRepository.delete(transaction);
    }

    public CardOwnedBy buyCard(BuyCardDTO buyCardDTO)
    {
        Transaction transaction = transactionRepository
                .findById(buyCardDTO.getTransactionId())
                .orElseThrow(()->new RessourceNotFoundException("La transaction n'existe pas"));

        User buyer = userRepository
                .findById(buyCardDTO.getBuyerId())
                .orElseThrow(()-> new RessourceNotFoundException("L'utilisateur n'existe pas"));

        if(buyer.getCredits() < transaction.getPrice())
        {
            throw new CreditException("L'utilisateur n'a pas les fonds suffisants pour acheter la carte");
        }

        User seller = transaction.getSeller();

        CardOwnedBy cardOwnedBy = cardOwnedByRepository
                .findById(transaction.getCard().getId())
                .orElseThrow(()-> new RessourceNotFoundException("L'exemplaire de la carte n'existe pas"));


        cardOwnedBy.setOwner(buyer);
        cardOwnedByRepository.save(cardOwnedBy);

        transaction.setBuyer(buyer);
        transaction.setTransactionTime(LocalDateTime.now());

        transactionRepository.save(transaction);

        buyer.setCredits(buyer.getCredits() - transaction.getPrice());
        seller.setCredits(seller.getCredits() + transaction.getPrice());


        userRepository.save(buyer);
        userRepository.save(seller);

        return cardOwnedBy;
    }
}
