package com.sp.service;

import com.sp.exception.RessourceNotFoundException;
import com.sp.model.User;
import com.sp.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(Long id)
    {
        User user = userRepository.findById(id).orElseThrow(() -> new RessourceNotFoundException("L'utilisateur n'existe pas"));

        return user;
    }
    public User createUserIfNotExists(String firstName, String lastName,String password) {

        User user = userRepository.findByFirstNameAndLastName(firstName,lastName);
        if(  user != null ){

            return user;
        }else {
            User newUser = new User(firstName, lastName, password);
            newUser.setCredits(500);

            return userRepository.save(newUser);

        }


    }
}
