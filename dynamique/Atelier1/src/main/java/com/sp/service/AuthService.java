package com.sp.service;

import com.sp.model.User;
import com.sp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    private UserRepository userRepository;

    public User registerUser(User user) {
        user.setCredits(50);
        return userRepository.save(user);
    }

    public User loginUser(String firstName, String password) {
        User user = userRepository.findByFirstName(firstName);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        else return null;
    }
}
