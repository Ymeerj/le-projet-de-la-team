package com.sp.service;

import com.sp.model.*;
import com.sp.repository.CardOwnedByRepository;
import com.sp.repository.CardRepository;
import com.sp.repository.TransactionRepository;
import com.sp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class CardService {
    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private CardOwnedByRepository cardOwnedByRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;




    public CardService(CardRepository cardRepository, UserRepository userRepository, TransactionRepository transactionRepository, CardOwnedByRepository cardOwnedByRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
        this.cardOwnedByRepository = cardOwnedByRepository;
    }

    public Card createCard(Card card) {
        return cardRepository.save(card);
    }

    public void sellCard(Long userId, Long cardId) {
        Transaction transaction = transactionRepository.findByBuyerIdAndCardId(userId, cardId);
        transactionRepository.delete(transaction);
    }
    public void assignRandomCardsToUser(Long userId) {
        List<Card> allCards = cardRepository.findAll();
        Random random = new Random();
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));

        for (int i = 0; i < 5; i++) {
            Card randomCard = allCards.get(random.nextInt(allCards.size()));
            CardOwnedBy cardOwnedBy = new CardOwnedBy(randomCard, user);
            cardOwnedBy.setOwner(user);
            cardOwnedByRepository.save(cardOwnedBy);
        }


    }

    public List<CardOwnedBy> getUserCard(Long userId){
        List<CardOwnedBy> userCards = cardOwnedByRepository.findByOwnerId(userId);
        List<Transaction> userTransactions = transactionRepository.findBySellerIdAndBuyerIsNull(userId);

        ArrayList<CardOwnedBy> finalCards = new ArrayList();
        for(CardOwnedBy cardOwnedBy: userCards)
        {
            boolean isInSell = false;
            for(Transaction transaction: userTransactions){
                if(transaction.getCard().getId() == cardOwnedBy.getId()){
                    isInSell = true;
                }
            }
            if(!isInSell) finalCards.add(cardOwnedBy);
        }
        return finalCards;

    }

    public Card createCardIfNotExists(String name, String description, String image_url) {
        Card card  = cardRepository.findByName(name);
        if (card == null) {
            Card newCard = new Card();
            newCard.setName(name);
            newCard.setDescription(description);
            newCard.setImage_url(image_url);
            return cardRepository.save(newCard);
        }
        return card;
    }
    public void assignCardToUserIfNotExists(Card card, User user) {
       List<CardOwnedBy> cardOwnedBy = cardOwnedByRepository.findCardOwnedByCardIdAndOwnerId(card.getId(), user.getId());
        if (cardOwnedBy.isEmpty()) {
            CardOwnedBy newCardOwnedBy = new CardOwnedBy(card, user);
            cardOwnedByRepository.save(newCardOwnedBy);
        }
    }
}
